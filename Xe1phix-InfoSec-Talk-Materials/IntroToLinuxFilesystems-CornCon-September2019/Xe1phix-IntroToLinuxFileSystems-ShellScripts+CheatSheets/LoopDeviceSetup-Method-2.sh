
## ============================= ##
echo "Linux with losetup Method 2"
## ============================= ##
dd if=/dev/urandom of=/usr/vdisk.img bs=1024k count=1024		# Creates the loop img 
#####################################################################################
losetup /dev/loop0 /usr/vdisk.img	# Creates and associates /dev/loop0 
mkfs.ext3 /dev/loop0 				# make a ext3 loop in dev
mount /dev/loop0 /mnt 			# mount loop on mnt
losetup ‐a                      # Check used loops 
umount /mnt 					# unmount loop
losetup ‐d /dev/loop0           # Detach 
rm /usr/vdisk.img				# remove loop image
#####################################################################################

