





## get the superblock information for the problem filesystem:
dumpe2fs /dev/sda2 | grep superblock


## sets the interval between checks to 13 days.
tune2fs -i 13d /dev/sda1




## use hashed b-trees to speed up directory lookups:
tune2fs -O '+' dir_index /dev/sda1


## Limit the number of backup superblocks to save space on large filesystems. 
tune2fs -O '+' sparse_super /dev/sda1


## List the contents of the filesystem superblock
tune2fs -l /dev/sda1


tune2fs -o 


## Enable debugging code for this filesystem.
tune2fs -o debug /dev/sda1


## Enable user-specified extended attributes.
tune2fs -o user_xattr /dev/sda1



tune2fs -o acl /dev/sda1
## Enable Posix Access Control Lists.


## Enable extra checks to be performed after reading or writing from the file system.  
## This prevents corrupted metadata blocks from causing file system damage 
## by overwriting parts of the inode table or block group descriptors. 
tune2fs -o block_validity /dev/sda1





## Allow  the value of each extended attribute to be placed 
## in the data blocks of a separate inode if necessary, 
## increasing the limit on the size and number of extended attributes per file.
tune2fs -O +ea_inode /dev/sda1


## Enable file system level encryption.
tune2fs -O +encrypt /dev/sda1


## Enable the use of extent trees to store the location of data blocks in inodes.
tune2fs -O +extent /dev/sda1


## Enable the extended inode fields used by ext4.
tune2fs -O +extra_isize /dev/sda1


## Store file type information in directory entries.
tune2fs -O +filetype /dev/sda1


## Filesystem can contain files that are greater than 2GB.
tune2fs -O +large_file /dev/sda1



## Store a checksum to protect the contents in each metadata block.
tune2fs -O +metadata_csum /dev/sda1


## Allow  the filesystem to store the metadata checksum seed in the superblock, 
## enabling the administrator to change the UUID of a filesystem 
## using the metadata_csum feature while it is mounted.
tune2fs -O +metadata_csum_seed /dev/sda1



## Enable or disable multiple mount protection (MMP) feature.
tune2fs -O +mmp /dev/sda1


## Enable internal file system quota inodes.
tune2fs -O +quota /dev/sda1


## Force the kernel to mount the file system read-only.
tune2fs -O +read-only /dev/sda1



## Limit the number of backup superblocks to save space on large filesystems.
tune2fs -O +sparse_super /dev/sda1



## Sets 'quota' feature on the superblock and works on the quota files for the given quota  type. 
tune2fs -Q +usrquota /dev/sda1
tune2fs -Q +grpquota /dev/sda1



## Set or clear the default mount options in the filesystem. 
tune2fs -E mount_opts=acl,user_xattr /dev/sda



## Change the behavior of the kernel code when errors are detected.
## 
## Remount filesystem read-only.
tune2fs -e remount-ro /dev/sda1


## Set  the  user  who can use the reserved filesystem blocks.
tune2fs -u 1000 /dev/sda1
tune2fs -u xe1phix /dev/sda1


## 
ls -l /dev/disk/by-uuid


## 
uuidgen


## 
tune2fs -U 79fb806d-4350-4b8c-8bc3-f3bb0c6b56f2 /dev/sdc1


## 
mount -U 79fb806d-4350-4b8c-8bc3-f3bb0c6b56f2 /mnt/clonedisk


umount /dev/sda1
tune2fs -O extents,uninit_bg,dir_index /dev/sda1
fsck -pf /dev/sda1
mount -t ext4 /dev/sda1



## resize an XFS filesystem on the fly:
xfs_growfs /filesystempath





## list the files and directory names in a particular image:
fls hdaimage.dd -r -f ext3 -i raw



## write files to a specified output directory:
foremost -t all -o /$Dir/ -i image.dd




## Pass in one of the backup superblocks as an option to fsck:
fsck -b 32768 /dev/hda2


## passing the backup superblock into mount explicitly:
mount sb=32768 /dev/hda2 /mnt


dd if=/dev/hda of=/mnt/recovery/hdaimage.dd



gddrescue -n /dev/hda /mnt/recovery/hdaimage.raw rescued.log


## grab most of the error-free areas quickly




## Use ddrescue, skipping the dodgy areas
## grab most of the error-free areas quickly
gddrescue -n /dev/hda /mnt/recovery/hdaimage.raw rescued.log



## mount the image as a loopback device:
mount -o loop /mnt/recovery/hdaimage.dd /mnt/hdaimage



## -r 1 instead of -n to 

## get as much as possible of the bad patches



## Find out where the partitions are:
## list the start and end cylinders of each partition 
## and the units in which they’re measured.
fdisk -lu /mnt/recovery/hdaimage.dd



## write the image back onto another disk:
dd if=/mnt/recovery/hdaimage.raw of=/dev/hdb


## get the superblock information
dumpe2fs /dev/hda2 | grep superblock


## Pass in one of the backup superblocks as an option to fsck:
fsck -b 32768 /dev/hda2

## passing the backup superblock into mount explicitly:
mount sb=32768 /dev/hda2 /mnt



## When the superblock of a partition is damaged, 
## you can specify a different superblock to use:
debugfs -b 1024 -s 8193 /dev/hda1



## see which filesystems are mounted by issuing the command:
cat /proc/mounts


## Find out what filesystems your current kernel supports:
cat /proc/filesystems



## Query the mount options:
tune2fs ‐l /dev/sdXY | grep "Default mount options:"


## add the ACL option to the disk:
tune2fs ‐o acl /dev/sda


## 
systool -b scsi


## 
lsscsi -s


## Find disks by their name
find /sys/devices/ -name scan


## ls the disks found using find to scan for disk names
ls -l $( find /sys/devices/ -name scan )

## 
find /sys -name sda

## 
ls -ldF $( find /sys -name sda )

## 
tree /sys/block/sda
tree /sys/class/block/sda
tree /sys/devices/pci0000:00/0000:00:11.0/ata1/host0/target0:0:0/0:0:0:0/block/sda/

## 
tree /sys/block

## 
tree -L 2 -F /sys/bus/pci*


## 
lsusb -v -s 004:018


## 
more /sys/class/net/p3p1/{carrier,duplex,speed}

## 
more /sys/class/net/[ep]*/statistics/[rt]x_bytes

## 
more /sys/class/net/[ep]**/mtu


## 
tree -F /dev/disk


## 
/dev/bus/usb/*







## find the logical sector size:
cat /sys/block/sda/queue/logical_block_size

## find the physical sector size:
cat /sys/block/sda/queue/physical_block_size

## find the physical sector size:
blockdev --getpbsz /dev/sda

## find the logical sector size:
blockdev --getss /dev/sda



## set a disk to read-only by setting a kernel flag
hdparm -r1 /dev/sdk

blockdev --setro /dev/sdk


mount -o ro







## specify the 4096-byte sector size with the -b flag
mmls -b 4096 /dev/sde







## Create A Ext4 Filesystem, specifying a block size of 4096-byte blocks:
mkfs.ext4 -b 4096 /dev/sde1

## Create an XFS FileSystem, specifying a block size of 8192-byte blocks:
mkfs.xfs -b size=8192 /dev/sde1



## Create a Btrfs FileSystem, using a custom node size or tree block size:
mkfs.btrfs -n 65536 /dev/sde1









## XFS Fragmentation and Defragmentation
## Measure the current level of fragmentation with xfs_db.
xfs_db -c frag -r /dev/sdb1


## Defragment XFS with xfs_fsr.
xfs_fsr /dev/sdb1


## 
xfs_repair -v /dev/sda3













## defragment a file system or individual files and directories.
btrfs filesystem defragment /home
btrfs filesystem defragment /usr/local/ISOs/*.iso



## 
cat /etc/fstab




## create a journal for an ext2 filesystem is as follows:
tune2fs -j /dev/hda1


## set the number of mounts between checks to 0
tune2fs -c 0 /dev/hda1



## create a journal of size 512MB:
tune2fs -j -J size=512 /dev/hda1


## create a journal file on a different device (here, /dev/hdb2) with this:
mke2fs -O journal_dev /dev/hdb2


## use this external journal file:
tune2fs -j -J device=/dev/hdb2 /dev/hda1


echo "display journal messages"
journalctl --list-boots | head



mkdir /var/log/journal
chgrp systemd-journal /var/log/journal
chmod 2775 /var/log/journal
systemctl restart systemd-journald.service
setfacl -Rnm g:wheel:rx,d:g:wheel:rx,g:adm:rx,d:g:adm:rx /var/log/journal/




## =========================================== ##
##   Enabling persistent logging In journald
## =========================================== ##

## To enable persistent logging, create /var/log/journal:
mkdir -p /var/log/journal
systemd-tmpfiles --create --prefix /var/log/journal

## systemd will make the journal files owned by the "systemd-journal" group and
## add an ACL for read permissions for users In the "adm" group




## Boot with these kernel command line options:
systemd.log_level=debug systemd.log_target=kmsg log_buf_len=1M





## mount a file system with write barriers disabled 
barrier=0    ## Ext4, 
nobarrier    ## XFS and Btrfs



data=ordered            ## Data blocks are written to the file system first, then metadata.
data=writeback          ## journals the metadata, and then data.
data=journal            ## writes all data twice: first to the journal and then to the data and metadata blocks.


## 
tune2fs -O ^has_journal /dev/sde1





## 
ls -l /dev/sd*

## 
ls -l /dev/mapper/




## 
pvscan

## 
vgscan

## 
pvdisplay

## 
vgdisplay



## 
swapon -s




## hex dump of a disk partition which is part of a logical volume
hexdump -C /dev/sdb2


## 
fdisk -l /dev/sda | grep /dev/sda



## 
grep vg_ /etc/fstab


## 
lvcreate -n music -L 100M myvg0





## mount a partition read-only
mount -t ext3 -o ro /dev/sdb1 /mnt/tmp


## remount /dev/sdb1 as read/write,
mount -t ext3 -o remount,rw /dev/sdb1












## 
mount -t debugfs none /sys/kernel/debug


## Detailed traces can be generated using ftrace:
echo 1 >/sys/kernel/debug/tracing/events/kvm/enable



cat /sys/kernel/debug/tracing/trace_pipe





## unmount the guest file system using fusermount.
fusermount -u /tmp/guestmount/24764

## 
perf kvm --host --guest --guestkallsyms=/tmp/guest.kallsyms --guestmodules=/tmp/guest.modules record -a



mkdir -p /tmp/guestmount
sshfs -o allow_other,direct_io guest:/ /tmp/guestmount/24764


perf kvm --host --guest --guestmount=/tmp/guestmount




## generate a report from a data file created by perf kvm record.
perf kvm --host --guestmount=/tmp/guestmount report


## generate a report for the guest, use the --guest argument.
perf kvm --guest --guestmount=/tmp/guestmount report


## 
perf kvm --host --guestmount=/tmp/guestmount report -i /tmp/perf.data.kvm






## --------------------------------------------------------------------------------------- ##
##   [+] Loop  Device - A block device that maps its data blocks to 
##       the blocks of a regular file in a filesystem or to another block device.
## --------------------------------------------------------------------------------------- ##
##   [?] Unlike other block devices, who maps their data block to a physical device 
##       such as a hard disk, optical disk drive, etc../ 
## --------------------------------------------------------------------------------------- ##

## Mounting a disk image in loopback
mkdir /mnt/kali
mount -o loop,ro /$Dir/kali.iso /mnt/kali


## 
mount -t iso9660 -o ro,loop=/dev/loop0 /home/xe1phix/parrotsec.iso /cdrom






## Linux loop­back
mount ‐t iso9660 ‐o loop file.iso /mnt          ## Mount a CD image
mount ‐t ext3 ‐o loop file.img /mnt             ## Mount an image with ext3 fs





dd if=/dev/zero of=/usr/vdisk.img bs=1024k count=1024
mkfs.ext3 /usr/vdisk.img
mount ‐o loop /usr/vdisk.img /mnt
umount /mnt; rm /usr/vdisk.img



## Linux with losetup
## /dev/zero is much faster than urandom , but less secure for encryption.
dd if=/dev/urandom of=/usr/vdisk.img bs=1024k count=1024
losetup /dev/loop0 /usr/vdisk.img


## Creates and associates /dev/loop0
mkfs.ext3 /dev/loop0
mount /dev/loop0 /mnt
losetup ‐a                      # Check used loops
umount /mnt
losetup ‐d /dev/loop0           ## Detach
rm /usr/vdisk.img



## 
mount ‐t tmpfs ‐osize=64m tmpfs /memdisk




## Read and write a 1 GB file on partition ad4s3c (/home)
time dd if=/dev/ad4s3c of=/dev/null bs=1024k count=1000
time dd if=/dev/zero bs=1024k count=1000 of=/home/1Gb.file
hdparm ‐tT /dev/hda


















## Backup and restore
dd if=/dev/hda of=/dev/hdc            ## Copy disk to disk (same size)
dd if=/dev/sda7 of=/home/root.img bs=4096 conv=notrunc,noerror      ## Backup /
dd if=/home/root.img of=/dev/sda7 bs=4096 conv=notrunc,noerror      ## Restore /

dd bs=1M if=/dev/sda | gzip ‐c > sda.gz                             ## Zip the backup
gunzip ‐dc ad4s3e.gz | dd of=/dev/ad0s3e bs=1M                      ## Restore the zip

dd bs=1M if=/dev/sda | gzip | ssh eedcoba@fry 'dd of=sda.gz'  ## remote
gunzip ‐dc sda.gz | ssh eedcoba@host 'dd of=/dev/sda bs=1M'




## Check for bad blocks
dd if=/dev/hda of=/dev/null bs=1m

## Send to remote machine
dd bs=1k if=/dev/hda1 conv=sync,noerror,notrunc | gzip | ssh root@fry 'dd of=hda1.gz bs=1k'

## Store into an image
dd bs=1k if=/dev/hda1 conv=sync,noerror,notrunc of=hda1.img

## Mount an image
mount ‐o loop /hda1.img /mnt

## Copy to a new disk
rsync ‐ax /mnt/ /newdisk/





##-================================================================================-##
##  [+] MBR tricks
##-================================================================================-##
## -------------------------------------------------------------------------------- ##
##  [?] The MBR contains the boot loader and the partition table and is 512 bytes
##  [?] The first 446 are for the boot loader, 
##  [?] bytes 446 to 512 are for the partition table.
## -------------------------------------------------------------------------------- ##


## ---------------------------------------------------------------------------------------------------------- ##
    dd if=/dev/sda of=/mbr_sda.bak bs=512 count=1               ## Backup the full MBR
## ---------------------------------------------------------------------------------------------------------- ##
    dd if=/dev/zero of=/dev/sda bs=512 count=1                  ## Delete MBR and partition table
## ---------------------------------------------------------------------------------------------------------- ##
    dd if=/mbr_sda.bak of=/dev/sda bs=512 count=1               ## Restore the full MBR
## ---------------------------------------------------------------------------------------------------------- ##
    dd if=/mbr_sda.bak of=/dev/sda bs=446 count=1               ## Restore only the boot loader
## ---------------------------------------------------------------------------------------------------------- ##
    dd if=/mbr_sda.bak of=/dev/sda bs=1 count=64 skip=446       ## Restore partition table
## ---------------------------------------------------------------------------------------------------------- ##






## write the image back onto another disk:
dd if=/mnt/recovery/hdaimage.raw of=/dev/hdb








# Count ext4 events for the entire system, for 10 seconds:
perf stat -e 'ext4:*' -a sleep 10




# Trace all block device (disk I/O) requests with stack traces, until Ctrl-C:
perf record -e block:block_rq_insert -ag

# Trace all block device issues and completions (has timestamps), until Ctrl-C:
perf record -e block:block_rq_issue -e block:block_rq_complete -a

# Trace all block completions, of size at least 100 Kbytes, until Ctrl-C:
perf record -e block:block_rq_complete --filter 'nr_sector > 200'

# Trace all block completions, synchronous writes only, until Ctrl-C:
perf record -e block:block_rq_complete --filter 'rwbs == "WS"'

# Trace all block completions, all types of writes, until Ctrl-C:
perf record -e block:block_rq_complete --filter 'rwbs ~ "*W*"'

# Trace all minor faults (RSS growth) with stack traces, until Ctrl-C








## use mmls to examine NVME disks:
mmls /dev/nvme0n1


## shows various information about NVMe namespace support:
nvme id-ctrl /dev/nvme1 -H



## check the size of the namespace
nvme id-ns /dev/nvme0n1




## list the associated files and paths for attached devices:
udevadm info /dev/sdf



## image a disk into a SquashFS forensic evidence container
sfsimage -i /dev/sde kingston.sfs


## add additional evidence to a container using sfsimage
sfsimage -a photo.jpg kingston.sfs



## list the contents of a SquashFS forensic evidence container
sfsimage -l kingston.sfs


## the *.sfs file is mounted with the -m flag
sfsimage -m kingston.sfs

## 
mmls kingston.sfs.d/image.raw



## unmount it with the -u flag:
sfsimage -u kingston.sfs.d





## 
time dcfldd if=/dev/sdc of=./ssd-image.raw







## view the storage interfaces, type (SATA, NVME, SCSI
lshw -class storage


lshw -businfo

lshw -businfo -class storage
lshw -businfo -class disk

## view the speed, interface, cache, and rotation about the attached disks:
hdparm -I /dev/sda


## enumerates all the SCSI, IDE, RAID, ATA, SATA, SAS, and NVME mass storage controller devices on a system:
for i in 00 01 04 05 06 07 08; do lspci -d ::01$i; done



## lists all SATA mass storage controller (class ID 01, subclass ID 06) devices:
lspci -d ::0106



## enumerates all FireWire, USB, and Fibre Channel serial bus controllers:
for i in 00 03 04; do lspci -d ::0C$i; done



## query for an attached storage device
## (SATA, USB, SAS, FireWire, ATA, SCSI, and Fibre Channel)
lsscsi -v



## Query lsblk for useful technical details
## (device name, size, physical and logical sector size, 
## transport (USB, SATA, SAS, etc), SCSI address, and more:
lsblk -pd -o TRAN,NAME,SERIAL,VENDOR,MODEL,REV,WWN,SIZE,HCTL,SUBSYSTEMS,HCTL


## Extract SMART Data with smartctl
smartctl -x /dev/sda




## disable copy-on-write for single files/directories do:
chattr +C /dir/file




## recover all known file types. 
foremost -t all -i /path/to/image -o outputdir




## clone a partition from physical disk /dev/sda, partition 1, to physical disk /dev/sdb, partition 1 with e2image, run
e2image -ra -p /dev/sda1 /dev/sdb1



## clone a faulty or dying drive, run ddrescue twice. First round, copy every block without read error and log the errors to rescue.log.
ddrescue -f -n /dev/sdX /dev/sdY rescue.log



## copy only the bad blocks and try 3 times to read from the source before giving up.
ddrescue -d -f -r3 /dev/sdX /dev/sdY rescue.log








Advanced Forensics Format (AFF)
Advanced Forensic Framework 4 (AFF4)
Expert Witness Compression Format (EWF)
Raw Image Format
Open Virtualization Format (OVF)
QCOW Image Format
Virtual Disk Image (VDI)
Virtual Hard Disk (VHD)
VMWare Virtual Disk Format (VMDK)



## Imaging a device on a Unix-based system:
ewfacquire /dev/sda


## Converting a RAW into an EWF image
ewfacquire myfile.raw


## 
ewfacquire -c best -m fixed -t myfile -S 1T -u [-q] myfile.raw


## 
cat split.raw.??? | ewfacquirestream
cat myfile.??? | ewfacquirestream  -c best -m fixed -t myfile -S 1T 


## Converting an optical disc (split) RAW into an EWF image (libewf 20110109 or later)
ewfacquire -T optical.cue optical.iso


## Converting an EWF into another EWF format or a (split) RAW image
ewfexport image.E01


## Exporting files from a logical image (L01)
ewfexport image.L01


## FUSE mounting an EWF image (libewf 20110828 or later)
ewfmount image.E01 mount_point


## FUSE mounting a logical image (L01) (libewf 20111016 or later)
ewfmount -f files image.L01 mount_point


## Verify an single image with results to the screen
ewfverify image.E01







## 
dd_rescue /dev/hda myfile.img



## add compression to an image file on the fly.
dd_rescue /dev/sda1 - | bzip2 > /dir/file.img.bz2




## read ten Gigabytes from the source drive and write that to a file 
## and the sha256 hash of the ten Gigabyte chunk. 
dcfldd if=/dev/sda hash=sha256 hashwindow=10G sha256log=sha256.txt hashconv=after bs=512 conv=noerror,sync split=10G splitformat=aa of=driveimage.dd



## mount a QCOW image:
qcowmount image.qcow2 /mnt/qcowimage/


## Pass "allow_root" to the fuse sub system using the qcowmount -X option:
qcowmount -X allow_root image.qcow2 /mnt/qcowimage/



unmount /mnt/qcowimage/ using umount:
umount /mnt/qcowimage/


## Or fusermount:
fusermount -u /mnt/qcowimage/






## create a new image, read/write snapshot of the original image:
qemu-img create -f qcow2 -b centos-cleaninstall.img snapshot.img


## determine an images backing file
qemu-img info snapshot.img

## [+] Temporary snapshots
## any changes made to the virtual machine while it is running are written to temporary files 
## and thrown away when the virtual machine is turned off
qemu -hda centos-cleaninstall.img -snapshot









## mount an overlay use the following mount options:
mount -t overlay overlay -o lowerdir=/lower,upperdir=/upper,workdir=/work /merged


## The lower directory can actually be a list of directories 
## separated by :, all changes in the merged directory are still reflected in upper.

mount -t overlay overlay -o lowerdir=/lower1:/lower2:/lower3,upperdir=/upper,workdir=/work /merged


## add an overlayfs entry to /etc/fstab use the following format:

/etc/fstab

overlay /merged overlay noauto,x-systemd.automount,lowerdir=/lower,upperdir=/upper,workdir=/work 0 0



## Read-only overlay
mount -t overlay overlay -o lowerdir=/lower1:/lower2 /merged
























