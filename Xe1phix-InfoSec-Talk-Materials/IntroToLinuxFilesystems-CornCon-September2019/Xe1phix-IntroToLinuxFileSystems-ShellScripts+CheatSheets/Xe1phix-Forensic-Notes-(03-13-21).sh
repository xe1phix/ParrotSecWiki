echo "################################"
echo "########## stegdetect ##########"
echo "################################"

stegdetect $FILE


echo "##############################"
echo "########## steghide ##########"
echo "##############################"

steghide extract -sf $FILE -p ""


echo "##############################"
echo "########## outguess ##########"
echo "##############################"

outguess -r $FILE $TMP_FILE
check_result_file $TMP_FILE


stegoveritas.py $FILE -outDir $TMP_DIR -meta -imageTransform -colorMap -trailing



identify -verbose stego.jpg


## Carve out embedded/appended files
foremost stego.jpg


ffmpeg -v info -i stego.mp3 -f null     ## to recode the file and throw away the result




#Log fls
sudo fls /forensic/floppy$1/forensic$1.iso > /forensic/floppy$1/logs/logfls$1.txt

#Change directory
cd floppy$1

#acquire file E01
yes "" | sudo ewfacquire /dev/fd0 -D floppy$1 -e floppysic -C 1.1 -N floppy$1 -E $1 -D floppy$1 -t floppy$1 -l ~/forensic/floppy$1/logs/ewfacquire$1.txt -m removable -M physical -f encase6 -c deflate -o 0 -B 737280 -S 1.4 -P 512 -g 64 -b 64 -w

#List files and directories in disk image
fls -r -m "/" -i ewf floppy$1.E01 >> ewf$1.txt

#Make timeline in csv format
mactime -b ewf$1.txt -d > mactime$1.csv


#Search file 
fls -r -F floppy2.E01 |grep


#Take back node 28 
icat floppy2.E01 28 > test22.PAS







ffmpeg -i contaminated.mov -acodec copy -vcodec copy clean.mov

ffmpeg -v info -i stego.mp3 -f null     ## to recode the file and throw away the result



Generate a timeline

log2timeline.py --status_view window --hashers MD5 ${TZ} ${DATADIR}/${HOSTNAME}.pb ${MOUNTPOINT}


Parse the MFT into a bodyfile format
2. analyzeMFT.py --bodyfull -b ${DATADIR}/${HOSTNAME}.bodyfile -f ${MOUNTPOINT}/$MFT


Read the bodyfile into the existing Plaso file
log2timeline.py ${DATADIR}/${HOSTNAME}.pb ${DATADIR}/${HOSTNAME}.bodyfile


Post process the plaso timeline, apply various analyses, and output to t2tcsv to be fed to logstash
psort.py --tagging-file /usr/share/plaso/tag_windows.txt ${TZ} -o l2tcsv -w ${DATADIR}/${HOSTNAME}.csv ${DATADIR}/${HOSTNAME}.pb


grep -E -o "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)" strings.txt





ls -1d /proc/fs/ext*/* /proc/fs/xfs*/* | awk -F/ '{ print $NF }' | while read PART; do
  echo /dev/$PART
done | while read DEV; do
  fls -m/ -r $DEV >> timeline.body
done

log2timeline.py timeline.pb timeline.body

psort.py -o l2tcsv -w timeline.csv timeline.pb



# Create new profile of Linux for use in Volatility
#
VOLHOME=~/volatility
OSNAME="myos"
 cd $VOLHOME/tools/linux && make
 zip /usr/local/lib/python2.7/dist-packages/volatility/plugins/overlays/$OSNAME.zip \
    /boot/System.map-3.13.0-62-generic $VOLHOME/tools/linux/module.dwarf 




### reassembly of packets + splitting in smaller reassembled pcaps.
 tshark -o 'tcp.desegment_tcp_streams:TRUE' -r "$1" -T fields -e tcp.stream | sort -un | tr '\n' ' ' > streams
 rm -rf ${SDIR}; mkdir ${SDIR}
 for x in `cat streams`; do 
    tshark -F pcap -r "$1" -w "${SDIR}/${x}.pcap" tcp.stream eq $x
    echo "Finished stream ${x}"
  done

  
  
  
  
  use the Sleuthkit to make a timeline

fls –rd / > filedump.txt
mactime –b filedump.txt > filedump_mac.txt

  
VMTYPE=`vmdkinfo $file 2>/dev/null | grep 'Disk type:' | awk -F: '{ print $2 }'`
FILETYPE=`file $file | awk -F: '{ print $2 }' | sed 's?^ ??'`
vhdimount $file $vhdmt
vmdkmount $file $vmdkmt
  
  
mmls -Ma $file | grep 'Linux Logical' | while read FS; do
echo "Found partition of type Linux Logical Volume"
OFFSET=$((`echo $FS | awk '{ print $3 }' | sed 's?^0*??'`*512))
  
mmls -Ma $file | grep 'Linux (' | while read FS; do
echo "Found partition of type Linux Native"
OFFSET=$((`echo $FS | awk '{ print $3 }' | sed 's?^0*??'`*512))
  
echo "Mounting Linux partition $file offset $OFFSET as $mt"
mount -o ro,noload,offset=$OFFSET $file $mt
  
  
mount -t ntfs -o ro,show_sys_files,streams_interface=windows,offset=$OFFSET $file $mt





mount a VHD image:

vhdimount image.vhd /mnt/fuse
vhdimount -X allow_root image.vhd /mnt/fuse

mount -o loop,ro,offset=${OFFSET} /mnt/fuse/vhdi1 /mnt/file_system

umount /mnt/fuse
fusermount -u /mnt/fuse




ewfmount 'image location' 'mountpoint'


ewfmount $file $e01mt











vol.py -f memdump.vmem --profile=Win2008R2SP1x64_23418 imagecopy -O memdump.mem


strings -o -el memdump.mem > memdump.txt
strings -o  memdump.mem >> memdump.txt
egrep -iwF 'badguy.net|45.xxx.xxx.xxx' memdump.txt > badguy.txt
less badguy.txt



vol.py -f memdump.mem --profile=Win2008R2SP1x64_23418 yarascan -y /tmp/cobalt.yar

vol.py --plugins=/plugings/cobalt -f memdump.mem --profile=Win2008R2SP1x64_23418 cobaltstrikeconfig -p 5352


python vol.py -f ~/Desktop/win7_trial_64bit.raw imageinfo







Exposing images works with E01s, VMDKS, RAW, bitlocker, LVM

imount /path/to/VMDK





# Simple script for VirtuaBox memory extraction
# Usage: vboxmemdump.sh <VM name>

VBoxManage debugvm $1 dumpvmcore --filename=$1.elf

size=0x$(objdump -h $1.elf|egrep -w "(Idx|load1)" | tr -s " " |  cut -d " " -f 4)
off=0x$(echo "obase=16;ibase=16;`objdump -h $1.elf|egrep -w "(Idx|load1)" | tr -s " " |  cut -d " " -f 7 | tr /a-z/ /A-Z/`" | bc)
head -c $(($size+$off)) $1.elf|tail -c +$(($off+1)) > $1.raw


# Get debugfs stat for all inodes
debugfs -R "stat <$line>" "$myrootdrive" | cat >> "$SAVETO/full_inode_extract_output.txt" 



# Get list of deleted inodes.
get_deleted_files(){

myrootdrive=$(df | awk '{print $1}' | grep /)

debugfs -R "lsdel" "$myrootdrive" | cat > "$SAVETO/deleted_files.txt"






IMG_TYPE=$(img_stat $1 | grep "Image Type:")

xmount --in $ITYPE --out dd ${imm[@]} $MNTPNT


mount -t auto -o ro,loop,noauto,noexec,nodev,noatime,offset=$off,umask=222 $imm $BASE_IMG


icat -f $fs -o $offs $imm $ind > $outputdir/${f##*/}


fsstat -o $offs $imm | grep -ia "File System Type:\|cluster size\|block size\|sector size"


fls -f list


inode=$(ifind -f $fs -o $offs -d $d $imm)


ffind -f $fs -o $offs $imm $ind)
echo "FILE NAME: "$f


blkcat -f $fs -o $offs $imm $d | xxd -l $ss



