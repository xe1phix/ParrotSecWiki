#!/bin/bash





##-==================================================================-##
##   [+] First a master key needs to be created in base64 encoding:
##-==================================================================-##
openssl rand -base64 32 > key.b64
KEY=$(base64 -d key.b64 | hexdump  -v -e '/1 "%02X"')



##-==============================================================-##
##   [+] Each secret to be encrypted needs to have a
##       Random Initialization Vector generated. 
##       These do not need to be kept secret
##-==============================================================-##
openssl rand -base64 16 > iv.b64
IV=$(base64 -d iv.b64 | hexdump  -v -e '/1 "%02X"')



##-=======================================================================-##
##   [+] The secret to be defined can now be encrypted
##       in this case we're telling openssl to base64 encode the result
##       but it could be left as raw bytes if desired.
##-=======================================================================-##
SECRET=$(echo -n "letmein" | openssl enc -aes-256-cbc -a -K $KEY -iv $IV)



##-=========================================================================-##
##   [+] When launching QEMU, create a master secret pointing to key.b64
##       Then specify that to be used to decrypt the user Password.
##   [+] Pass the contents of "iv.b64" to the second secret
##-=========================================================================-##
$QEMU -object secret,id=secmaster0,format=base64,file=key.b64 -object secret,id=sec0,keyid=secmaster0,format=base64,data=$SECRET,iv=$(<iv.b64)



##-=======================================================================-##
##   [?] The simplest secure usage is to provide the secret via a file
##-=======================================================================-##
echo -n "letmein" > mypasswd.txt 
$QEMU -object secret,id=sec0,file=mypasswd.txt,format=raw





##-==================================================================-##
##   [+] 
##-==================================================================-##



