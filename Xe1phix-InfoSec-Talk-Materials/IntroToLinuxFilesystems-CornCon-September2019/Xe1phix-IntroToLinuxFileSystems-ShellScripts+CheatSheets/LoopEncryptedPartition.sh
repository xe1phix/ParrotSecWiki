#!/bin/bash
## LoopEncryptedPartition.sh 


## Creating an Empty Image File
$ dd if=/dev/urandom of=/home/bob/safe.img bs=1k count=10024

## Creating a Loop Device
$ losetup /dev/loop0 /home/bob/safe.img

## Creating Encrypted File System
$ cryptsetup -y create safe /dev/loop0

$ cryptsetup -c blowfish -h sha1 create safe /dev/loop0

# status information on your mapped devices.

cryptsetup status safe

# create an ext3 type file

mkfs.ext3 -j /dev/mapper/safe

mkdir /home/bob/safe

mount -t ext3 /dev/mapper/safe /home/bob/safe
