#!/bin/sh

##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##
##     [+] Create A GPG Key:
##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##
gpg --enable-large-rsa --full-gen-key


##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##
##    [+] Encrypts $File With A Symmetric Cipher Encryption (Using A Passphrase):			
##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##
## --------------------------------------------------------------------------------------------------------------------------- ##
##    [?] Uses The AES-256 Cipher Algorithm To Encrypt The Passphrase							 
##    [?] Uses The SHA-512 Digest Algorithm To Mangle The Passphrase							
##    [?] Mangles The Passphrase For 65536 Iterations															 
## --------------------------------------------------------------------------------------------------------------------------- ##
gpg --verbose --symmetric --cipher-algo aes256 --digest-algo sha512 --cert-digest-algo sha512 --s2k-mode 3 --s2k-count 65011712 --s2k-cipher-algo AES256 --s2k-digest-algo SHA512 $File


##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##
##     [?] The Owner Exports His GPG Public Key For The Recipient
##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##
gpg --export --armor Xe1phix > Xe1phix.asc


##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##
##     [+] Import A GPG Public Key:
##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##
gpg --keyid-format 0xlong --import Xe1phix.asc


##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##
##      [+] Encrypt & Sign A File (Using Xe1phix As The Recipient):       
##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##
gpg --encrypt --sign --armor --recipient Xe1phix@mail.i2p $File
gpg -se -r Xe1phix@mail.i2p $File


##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##
##     [+] Verify The Recipients Signature File Against The Base File:
##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##
gpg --verify --keyid-format 0xlong $file.txt.gpg $file.txt


