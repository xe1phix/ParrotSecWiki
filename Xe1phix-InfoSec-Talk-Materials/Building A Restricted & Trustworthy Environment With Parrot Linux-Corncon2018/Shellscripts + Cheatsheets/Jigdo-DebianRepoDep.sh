#!/bin/sh
## Jigdo-DebianRepoDep.sh

rm -f /etc/apt/sources.list.d/parrot.list
rm -f /etc/apt/sources.list.parrot
echo "deb http://deb.debian.org/debian stretch main contrib non-free" > /etc/apt/sources.list
echo "deb-src http://deb.debian.org/debian stretch main contrib non-free" >> /etc/apt/sources.list
echo "deb http://deb.debian.org/debian stretch-updates main contrib non-free" >> /etc/apt/sources.list
echo "deb-src http://deb.debian.org/debian stretch-updates main contrib non-free" >> /etc/apt/sources.list
echo "deb http://security.debian.org/debian-security/ stretch/updates main contrib non-free" >> /etc/apt/sources.list
echo "deb-src http://security.debian.org/debian-security/ stretch/updates main contrib non-free" >> /etc/apt/sources.list
echo "" >> /etc/apt/sources.list
echo "## deb http://deb.parrotsec.org/parrot stable main contrib non-free" >> /etc/apt/sources.list
echo "## deb-src http://archive.parrotsec.org/parrot stable main contrib non-free" >> /etc/apt/sources.list
echo
echo 
echo "##-================================-##"
echo "           Jigdo Repo Url:            "
echo "##-================================-##"
echo "http://httpredir.debian.org/debian/"
echo 

echo "##-================================-##"
echo "             Jigdo Syntax:            "
echo "##-================================-##"
echo "jigdo-lite $file.jigdo" 
