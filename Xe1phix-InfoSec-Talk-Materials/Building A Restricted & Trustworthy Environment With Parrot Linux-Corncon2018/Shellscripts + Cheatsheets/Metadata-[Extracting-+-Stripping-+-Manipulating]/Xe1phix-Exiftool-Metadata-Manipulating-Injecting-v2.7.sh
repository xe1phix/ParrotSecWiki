#!/bin/sh
##-==========================================================-##
##    Xe1phix-Exiftool-Metadata-Manipulating-Injecting.sh
##-==========================================================-##


## Extract Source Image Metadata That You're Copying:
exif img_1270.jpg
exiftags -idav img_1270.jpg
exifprobe img_1270.jpg
exiv2 -Pkyct img_1270.jpg
exiftool -verbose -extractEmbedded img_1270.jpg 
exiftool -a -G1 -simg_1270.jpg 

## extract all date/time information with an indication of where it is stored:
exiftool -time:all -a -G0:1 -s img_1270.jpg 
exiftool -a -u -g1 img_1270.jpg 


## Remove all Metadata from dst image (So none of the data overlaps)
exiftool -all=  1459391988884.png


## Copy the values of all writable tags from "src.jpg" to "dst.jpg"
exiftool -TagsFromFile $src.jpg -all:all $dst.jpg


## Erase all meta information from "dst.jpg" image
## then copy EXIF tags from "src.jpg".
exiftool -all= -tagsfromfile $src.jpg -exif:all $dst.jpg


## Copy all meta information from "a.jpg" to "b.jpg"
## delete all XMP information and the thumbnail image from the destination first.
exiftool -tagsFromFile $a.jpg -XMP:All= -ThumbnailImage= -m $b.jpg


## Copy meta information from "a.jpg" to an XMP data file.
exiftool -Tagsfromfile $a.jpg $out.xmp


## Copy all possible information from "src.jpg"
## write in XMP format to "dst.jpg".
exiftool -TagsFromFile $src.jpg '-all>xmp:all' $dst.jpg


## Copy ICC_Profile from one image to another.
exiftool -TagsFromFile $src.jpg -icc_profile $dst.jpg


## Copy the Make and Model tags, and implant them into another image:
exiftool -tagsfromfile $src.jpg -makernotes -make -model $dst.jpg


## copy all information and preserve the original structure
exiftool -tagsfromfile src.jpg -all:all dst.jpg


## copy XMP as a block from one file to another
exiftool -tagsfromfile src.jpg -xmp dst.cr2


echo "##-===================================================-##"
echo "   [+] Erase all meta information from dst.jpg image"
echo "	 		then copy EXIF tags from src.jpg"
echo "##-===================================================-##"
exiftool -all= -tagsfromfile src.jpg -exif:all dst.jpg



echo "##-=================================================================================-##"
echo "   [+] Copy all possible information from src.jpg and write in XMP format to dst.jpg."
echo "##-=================================================================================-##"
exiftool -TagsFromFile src.jpg '-all>xmp:all' dst.jpg


