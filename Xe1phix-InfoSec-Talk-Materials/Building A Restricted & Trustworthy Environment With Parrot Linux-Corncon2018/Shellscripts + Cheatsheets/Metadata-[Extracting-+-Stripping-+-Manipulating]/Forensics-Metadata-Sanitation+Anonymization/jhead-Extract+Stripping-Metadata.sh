#!/bin/sh


jhead -v '026 (copy).jpg'
jhead -exifmap '026 (copy).jpg'
jhead -exifmap '026 (copy).jpg' > 026-exifmap.txt


jhead -de               ## Strip Exif section (Delete the Exif header entirely.  Leaves other metadata sections intact.)
jhead -di               ## Delete IPTC section (from Photoshop, or Picasa)
jhead -dx               ## Deletex XMP section
jhead -purejpg          ## Strip all unnecessary data from jpeg (combines -dc -de and -du)


jhead -de '026 (copy).jpg'
jhead -di '026 (copy).jpg'
jhead -dx '026 (copy).jpg'
jhead -purejpg '026 (copy).jpg'

