#!/bin/sh
## ######## ##



echo "## ======================================== ##" > ~/.gnupg/gpg.conf
echo "## [?] use the strongest algorithms:" >> ~/.gnupg/gpg.conf
echo "## ======================================== ##" >> ~/.gnupg/gpg.conf

echo "## ======================================== ##"
echo "## [!] Editing The ~/.gnupg/gpg.conf File..."
echo "## ======================================== ##"

echo "## ==================================================================================================== ##" >> ~/.gnupg/gpg.conf
echo "personal-digest-preferences SHA512 SHA256" >> ~/.gnupg/gpg.conf
echo "cert-digest-algo SHA512" >> ~/.gnupg/gpg.conf
echo "default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES ZLIB BZIP2 ZIP Uncompressed" >> ~/.gnupg/gpg.conf
echo "personal-cipher-preferences AES256 AES192 AES" >> ~/.gnupg/gpg.conf
echo "## ==================================================================================================== ##" >> ~/.gnupg/gpg.conf

