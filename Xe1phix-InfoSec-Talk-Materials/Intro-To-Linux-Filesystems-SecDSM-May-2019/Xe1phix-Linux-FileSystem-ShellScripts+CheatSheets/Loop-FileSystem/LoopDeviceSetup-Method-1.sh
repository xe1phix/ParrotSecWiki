

## ============================== ##
echo "Linux with losetup Method 1"
## ============================== ##

#####################################################################################
dd if=/dev/zero of=/usr/vdisk.img bs=1024k count=1024 		# Creates the loop img
#####################################################################################
mkfs.ext3 /usr/vdisk.img				# make a ext3 loop in dev
mount ‐o loop /usr/vdisk.img /mnt		# mount loop in directory
umount /mnt; rm /usr/vdisk.img			# Cleanup unmount and remove
#####################################################################################

