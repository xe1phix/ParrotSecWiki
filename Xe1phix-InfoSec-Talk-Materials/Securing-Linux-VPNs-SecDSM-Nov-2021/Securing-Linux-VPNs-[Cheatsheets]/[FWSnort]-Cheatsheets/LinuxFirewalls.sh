#!/bin/sh
echo "##########################"
echo "## LinuxFirewalls.sh"
echo "##########################"
IPTABLES=/sbin/iptables
MODPROBE=/sbin/modprobe
INT_NET=192.168.10.0/24

echo "## ============================================== ##"
$IPTABLES -A INPUT 1 -s 144.202.X.X -j DROP
$IPTABLES -A OUTPUT 1 -d 144.202.X.X -j DROP
$IPTABLES -A FORWARD 1 -s 144.202.X.X -j DROP
$IPTABLES -A FORWARD 1 -d 144.202.X.X -j DROP
echo "## =========================================================== ##"
echo "### flush existing rules and set chain policy setting to DROP"
echo "[+] Flushing existing iptables rules..."
echo "## =========================================================== ##"
$IPTABLES -F
$IPTABLES -F -t nat
$IPTABLES -X
$IPTABLES -P INPUT DROP
$IPTABLES -P OUTPUT DROP
$IPTABLES -P FORWARD DROP
echo "## ================================ ##"
echo "### load connection-tracking modules"
$MODPROBE ip_conntrack
$MODPROBE iptable_nat
$MODPROBE ip_conntrack_ftp
$MODPROBE ip_nat_ftp
echo "## ================================ ##"
echo "###### INPUT chain ######"
echo "[+] Setting up INPUT chain..."
echo "## ================================================ ##"
echo "### state tracking rules"
$IPTABLES -A INPUT -m state --state INVALID -j LOG --log-prefix "DROP INVALID " --log-ip-options --log-tcp-options
$IPTABLES -A INPUT -m state --state INVALID -j DROP
$IPTABLES -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
echo "## =========================================================== ##"

echo "### anti-spoofing rules"
$IPTABLES -A INPUT -i eth1 -s ! $INT_NET -j LOG --log-prefix "SPOOFED PKT "
$IPTABLES -A INPUT -i eth1 -s ! $INT_NET -j DROP
echo "## =========================================================== ##"
echo "### ACCEPT rules"
$IPTABLES -A INPUT -i eth1 -p tcp -s $INT_NET --dport 22 --syn -m state --state NEW -j ACCEPT
$IPTABLES -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
echo "## =========================================================== ##"
echo "### default INPUT LOG rule"
$IPTABLES -A INPUT -i ! lo -j LOG --log-prefix "DROP " --log-ip-options --log-tcp-options


echo "## ========================= ##"
echo "### ==== OUTPUT chain ====== ##"
echo "## ========================= ##"
##
##
echo "## ================================= ##"
echo "[+] Setting up OUTPUT chain..."
echo "## ================================= ##"
echo "## "
echo "## state tracking rules"
echo "#########################"
$IPTABLES -A OUTPUT -m state --state INVALID -j LOG --log-prefix "DROP INVALID " --log-ip-options --log-tcp-options
$IPTABLES -A OUTPUT -m state --state INVALID -j DROP
$IPTABLES -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
echo "## =========================================================== ##"
echo "### ACCEPT rules for allowing connections out"
echo "## ============================================ ##"
$IPTABLES -A OUTPUT -p tcp --dport 21 --syn -m state --state NEW -j ACCEPT
$IPTABLES -A OUTPUT -p tcp --dport 22 --syn -m state --state NEW -j ACCEPT
$IPTABLES -A OUTPUT -p tcp --dport 25 --syn -m state --state NEW -j ACCEPT
$IPTABLES -A OUTPUT -p tcp --dport 43 --syn -m state --state NEW -j ACCEPT
$IPTABLES -A OUTPUT -p tcp --dport 80 --syn -m state --state NEW -j ACCEPT
$IPTABLES -A OUTPUT -p tcp --dport 443 --syn -m state --state NEW -j ACCEPT
$IPTABLES -A OUTPUT -p tcp --dport 4321 --syn -m state --state NEW -j ACCEPT
$IPTABLES -A OUTPUT -p udp --dport 53 -m state --state NEW -j ACCEPT
$IPTABLES -A OUTPUT -p tcp --dport 53 -m state --state NEW -j ACCEPT
$IPTABLES -A OUTPUT -p icmp --icmp-type echo-request -j ACCEPT
echo "## =========================================================== ##"
echo "### default OUTPUT LOG rule"
echo "## ======================================= ##"
$IPTABLES -A OUTPUT -o ! lo -j LOG --log-prefix "DROP " --log-ip-options --log-tcp-options

echo "## =========================================================== ##"
echo "###### FORWARD chain ######"
echo "## ======================================= ##"
echo "[+] Setting up FORWARD chain..."
echo "## ======================================= ##"
echo "### state tracking rules"
echo "#########################"
$IPTABLES -A FORWARD -m state --state INVALID -j LOG --log-prefix "DROP INVALID " --log-ip-options --log-tcp-options
$IPTABLES -A FORWARD -m state --state INVALID -j DROP
$IPTABLES -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT

echo "### anti-spoofing rules"
$IPTABLES -A FORWARD -i eth1 -s ! $INT_NET -j LOG --log-prefix "SPOOFED PKT "
$IPTABLES -A FORWARD -i eth1 -s ! $INT_NET -j DROP


echo "###########################################################################################"
echo "## ===================================================================================== ##"
echo "###########################################################################################"
echo "### ACCEPT rules"
echo "####################"
$IPTABLES -A FORWARD -p tcp -i eth1 -s $INT_NET --dport 21 --syn -m state
$IPTABLES -A FORWARD -p tcp -i eth1 -s $INT_NET --dport 22 --syn -m state
$IPTABLES -A FORWARD -p tcp -i eth1 -s $INT_NET --dport 25 --syn -m state
$IPTABLES -A FORWARD -p tcp -i eth1 -s $INT_NET --dport 43 --syn -m state
$IPTABLES -A FORWARD -p tcp --dport 80 --syn -m state --state NEW -j ACCEPT
$IPTABLES -A FORWARD -p tcp --dport 443 --syn -m state --state NEW -j ACCEPT
$IPTABLES -A FORWARD -p tcp -i eth1 -s $INT_NET --dport 4321 --syn -m state
$IPTABLES -A FORWARD -p udp --dport 53 -m state --state NEW -j ACCEPT
$IPTABLES -A FORWARD -p tcp --dport 53 -m state --state NEW -j ACCEPT
$IPTABLES -A FORWARD -p icmp --icmp-type echo-request -j ACCEPT

echo "###########################################################################################"
echo "## ===================================================================================== ##"
echo "###########################################################################################"
echo "##"
echo "### default log rule"
$IPTABLES -A FORWARD -i ! lo -j LOG --log-prefix "DROP " --log-ip-options --log-tcp-options

echo "###########################################################################################"
echo "## ===================================================================================== ##"
echo "###########################################################################################"
echo "### NAT rules"
echo "##########################"
echo "##"
echo "##"
echo "## ================================ ##"
echo "[+] Setting up NAT rules..."
echo "## ================================ ##"
$IPTABLES -t nat -A PREROUTING -p tcp --dport 80 -i eth0 -j DNAT --to 192.168.10.3:80
$IPTABLES -t nat -A PREROUTING -p tcp --dport 443 -i eth0 -j DNAT --to 192.168.10.3:443
$IPTABLES -t nat -A PREROUTING -p tcp --dport 53 -i eth0 -j DNAT --to 192.168.10.4:53
$IPTABLES -t nat -A POSTROUTING -s $INT_NET -o eth0 -j MASQUERADE
echo "###########################################################################################"
echo "## ===================================================================================== ##"
echo "###########################################################################################"


iptables -I INPUT 1 -p tcp --dport 15104 -j LOG --log-tcp-options --log-tcp-sequence
iptables -I OUTPUT 1 -d target -p tcp --tcp-flags RST RST -j DROP

iptables -I INPUT 1 -m limit --limit 10/sec -s 144.202.X.X -j ACCEPT
iptables -I INPUT 2 -s 144.202.X.X -j DROP
iptables -I OUTPUT 1 -m limit --limit 10/sec -d 144.202.X.X -j ACCEPT
iptables -I OUTPUT 2 -d 144.202.X.X -j DROP
iptables -I FORWARD 1 -m limit --limit 10/sec -s 144.202.X.X -j ACCEPT
iptables -I FORWARD 2 -s 144.202.X.X -j DROP
iptables -I FORWARD 1 -m limit --limit 10/sec -d 144.202.X.X -j ACCEPT
iptables -I FORWARD 2 -d 144.202.X.X -j DROP

echo "###########################################################################################"
echo "## ===================================================================================== ##"
echo "###########################################################################################"







iptables-save > /root/ipt.save
