## Lets Check The Integrity of The iso:


## Generate The SHA1, SHA256, and SHA512 Hashes:
sha1sum Parrot-home-4.2_amd64.iso

sha256sum Parrot-home-4.2_amd64.iso

sha512sum Parrot-home-4.2_amd64.iso


## Using OpenSSL, Generate The SHA1, SHA256, and SHA512 Hashes:

openssl dgst -md5 Parrot-home-4.2_amd64.iso

openssl dgst -sha256 Parrot-home-4.2_amd64.iso

openssl dgst -sha512 Parrot-home-4.2_amd64.iso


## To Verify The Hashes, Check The Site For A Signed Text File of The Hashes:
https://cdimage.parrotsec.org/parrot/iso/4.2/signed-hashes.txt
