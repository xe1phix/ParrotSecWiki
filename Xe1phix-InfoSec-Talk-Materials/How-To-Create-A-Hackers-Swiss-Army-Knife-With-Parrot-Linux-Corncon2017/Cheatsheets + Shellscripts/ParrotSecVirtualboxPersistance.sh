#!/bin/sh

echo "## =============================================================== ##"
echo "   [+] Associate the empty space with the sda3 partition"
echo "## =============================================================== ##"
gparted &
format sda1 to ext4


echo "## ======================================================================= ##"
echo "   [+]  Initialize the LUKS encryption on the newly-created partition."
echo "## ======================================================================= ##"
cryptsetup --verbose --verify-passphrase luksFormat /dev/sda1
cryptsetup luksOpen /dev/sda1 persistence


echo "## =============================================================== ##"
echo "   [+]  Create the ext3 filesystem, and label it persistence. "
echo "## =============================================================== ##"
mkfs.ext4 -L persistence /dev/mapper/persistence
e2label /dev/mapper/persistence persistence


echo "## ======================================================================= ##"
echo "   [+]  Create a mount point, mount our new encrypted partition there"
echo "   [+]  set up the persistence.conf file, and unmount the partition. "
echo "## ======================================================================= ##"
mkdir -p /mnt/persistence
mount /dev/mapper/persistence /mnt/persistence
echo "/ union" > /mnt/persistence/persistence.conf



echo "## =============================================================== ##"
echo "   [+] Status of the mapping (persistence) "
echo "## =============================================================== ##"
cryptsetup status /dev/mapper/persistence

echo "## =============================================================== ##"
echo "   [+]  Dump the header information of a LUKS device."
echo "## =============================================================== ##"
cryptsetup luksDump /dev/sda1



echo "## =============================================================== ##"
echo "   [+] Print the UUID of a LUKS device."
echo "## =============================================================== ##"
cryptsetup luksUUID /dev/sda1


echo "## =============================================================== ##"
echo "   [+] Add a Nuke Slot to /dev/sdc3: "
echo "## =============================================================== ##"
cryptsetup luksAddNuke /dev/sda1

echo "## =============================================================== ##"
echo "   [+] Check if the Nuke Slot has been added:"
echo "## =============================================================== ##"
cryptsetup luksDump /dev/sda1




echo "## =============================================================== ##"
echo "   [+] Stores a binary backup of the LUKS header and keyslot area."
echo "## =============================================================== ##"
cryptsetup luksHeaderBackup --header-backup-file luksheader.back /dev/sda1


echo "## =============================================================== ##"
echo "   [+] "
echo "## =============================================================== ##"
file luksheader.back

echo "## =============================================================== ##"
echo "   [+] Restores a binary backup of the LUKS header "
echo "   [+] and keyslot area from the specified file."
echo "## =============================================================== ##"
cryptsetup luksHeaderRestore /dev/sda1 --header-backup-file luksheader.back



cp -v ParrotSecPersistance.sh /mnt/persistence/


echo "## =============================================================== ##"
echo "   [+] Close the encrypted channel to our persistence partition."
echo "   [+]          and unmount the partition. "
echo "## =============================================================== ##"
umount /dev/mapper/persistence
cryptsetup luksClose /dev/mapper/persistence


