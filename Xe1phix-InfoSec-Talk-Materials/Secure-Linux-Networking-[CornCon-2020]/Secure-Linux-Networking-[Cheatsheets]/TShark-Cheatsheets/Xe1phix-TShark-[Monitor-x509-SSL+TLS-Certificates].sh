#!/bin/sh
##-=====================================-##
##     [+] Monitor x509 (SSL/TLS) Certificates:
##-=====================================-##
tshark -Y "ssl.handshake.certificate" -Tfields -e ip.src -e x509sat.uTF8String -e x509sat.printableString -e x509sat.universalString -e x509sat.IA5String -e x509sat.teletexString -Eseparator=/s -Equote=d
