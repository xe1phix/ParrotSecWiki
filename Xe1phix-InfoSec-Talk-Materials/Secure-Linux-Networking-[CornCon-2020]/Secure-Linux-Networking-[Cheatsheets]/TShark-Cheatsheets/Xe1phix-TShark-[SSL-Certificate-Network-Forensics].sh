#!/bin/sh
##-====================================================-##
##     [+] Xe1phix-TShark-[SSL-Certificate-Network-Forensics].sh
##-====================================================-##


##-============================================-##
##     [+] Only print the source IP and destination IP 
##          for all SSL handshake packets
##-============================================-##
tshark -r $File.pcap -Y "ssl.handshake" -Tfields -e ip.src -e ip.dst


##-==================================================-##
##     [+] List issuer name for all SSL certificates exchanged
##-==================================================-##
tshark -r $File.pcap -Y "ssl.handshake.certificate" -Tfields -e x509sat.printableString


##-======================================================-##
##     [+] Print the IP addresses of all servers accessed over SSL
##-======================================================-##
tshark -r $File.pcap -Y "ssl && ssl.handshake.type==1" -Tfields -e ip.dst

