ParrotSec-Building A Restricted & Trustworthy Environment
                (A laptop is required)

I will be giving a live demo of several Parrot Linux use cases. 
Here is a brief overview of the topics I will cover:
    _______________________________________________________________________________________________

    > Cryptography  - How to encrypt, decrypt, sign, and verify files with gpg and openssl.
    > Security      - How to use firejail to secure applications by sandboxing applications, 
                      restricting syscalls, enforcing apparmor, and blacklisting capabilities.
    > Forensics     - How to Carve & Analyze hidden file metadata with exiv2, exiftool, and jhead.
    > Antiforensics - How to encrypt a usb with LUKS encryption, create a killslot, and backup LUKS headers.

This talk is a live, and interactive workshop. I will be using the linux CLI for the entire talk. 
I will Give away preinstalled ParrotSec live DVDs To anyone with a laptop so they may follow along.

All of the syntax I execute will be thoroughly documented and explained.
Also, the code will be publicly accessible in my gitlab repository.
The topics covered in my talk will also have step by step instructional videos posted on my YouTube channel.
This way, the audience can replicate the steps I performed in the presentation. 

In depth overview of the topics covered in my talk:

Cryptography
I will explain GNUPG fundamentals, and its importance. Then I will demonstrate: 
> How create a GPG key
> How to zip a directory, and encrypt it.
> How to encrypt, decrypt, and sign (.asc, .gpg,) a file with GnuPG
> How to check the integrity of files
    > Verify the hashsum (.{sha1,sha256,sha512})
    > Verify the GPG signature (.{asc,gpg,sign,pub})
> How to configure your gpg.conf file into a hardened gpg.conf file.


Security
I will use firejail as a way to secure, confine, and restrict commonly used applications
I will include a link on gitlab of my most commonly used firejail profiles and command syntax.
> How to create a new network namespace and connect it to a bridged interface.
> how to enable Seccomp filtering (blacklisting defined syscalls).
> How to enable AppArmor security profile to enforce mode.
> How to Define a custom blacklist capabilities filter.
> How to Enable a new network namespace, and connect it to a bridged interface.
> How to force your dns resolver to use a trusted and hardened DNS nameserver.
> How to harden firefox's about:config (harden firefox config attributes).
> How to audit and debug an application for possible security flaws, or data leakage.


Forensics
For this topic, I will explain how imbedded metadata attributes work. And how to manipulate them. 
> How to forensically carve & analyze metadata
  > EXIF, GPS, IPTC, XMP, JFIF, GeoTIFF, ID3 etc
> EXIF/Geolocation Data Extracting & Anonymizing
  > GPSLatitude, GPSLongitude, etc
> How to manipulate metadata tags - How to copy metadata from one file, and inject it into anothers. 
> Anonymizing & sanitizing Metadata.


Antiforensics
Create a live bootable ParrotSec linux usb with a LUKS encrypted persistance partition.
> How to format the usb with Parrot Linux.
> How to add a Nuke Slot to the LUKS header.
> How to Create a binary backup of the LUKS header.
> How to Restore a binary backup of the LUKS header.
> How to use eCrypt to create an encrypted partition.
> How to securely wipe data (6 different methods).


I have been using parrot for 5 years, and volunteering within the community for the last 2 years now. 
I have written for the parrot wiki: 
(https://docs.parrotsec.org/doku.php/anonsurf) 
(original: https://github.com/xe1phix/ParrotSecWiki/blob/master/AnonSurf.txt)
(Parrot OS demo: https://www.youtube.com/watch?v=KZmXTnOlmME)

Thank you for considering my presentation
You can contact me at markrobertcurry@gmail.com

