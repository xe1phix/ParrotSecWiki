                            ##-================-##
                            ##   [+] Firejail
                            ##-================-##
## --------------------------------------------------------------------------- ##
##  [+] Default Firejail profile:
##        > Force use of secure DNS server
##        > Seccomp-BPF - syscall filtering
##        > Enable AppArmor confinement
##        > Linux capabilities filter (POSIX 1003.1e)
##        > Block access to external media
##        > User Namespaces
##        > Mount Namespaces
##        > Chroot containers
##        > PID Namespaces
##        > OverlayFS
##        > CGroups (Linux Control Groups)
##        > NoNewPrivs
##        > IPC Namespaces
##        > UTS Namespaces
##        > Bind mounts
##        > Whitelising Downloads folder, blacklisting everything else
##        > Restrict protocols
##           > Blacklist appletalk
##           > Blacklist IPv6
##  > Debugging Firejail profiles with STrace
##  > Auditing Firejail profile capabilities
##  > DBus sandboxing policies
## -------------------------------------------------------------------------- ##


                ##-==========================-##
                ##   [+] Kernel arguements
                ##-==========================-##
## ------------------------------------------------------- ##
##   [+] AppArmor
##        > security=apparmor + apparmor=1
##   [+] Kernel Self Protection Project
##        > init_on_alloc=1				## Wipe slab and page allocations
##        > init_on_free=1				## slub/slab allocator free poisoning
##        > randomize_kstack_offset=on  ## Randomize kernel stack offset on syscall entry
##        > pti=on						## Kernel Page Table Isolation
##        > nosmt						## prevent against L1TF
##        > slab_nomerge				## Disable slab merging
##   [+] IPv6 
##         > noipv6
##         > ipv6.autoconf=0
##         > ipv6.disable=0
##   [+] Modprobe.blacklist=
##         > appletalk
##         > bluetooth
##         > hfs + hfsplus
##         > efivarfs + efivars
##   [+] systemd.mask=
##         > mysql + mysqld + postgresql
##         > lighttpd + apache2 + rpcbind
## ------------------------------------------------------ ##


                ##-===========================-##
                ##   [+] Systemd Hardening
                ##-===========================-##
## ------------------------------------------------------ ##
##   [+] Systemd Device Hardening:
##         > PrivateDevices=yes
##         > PrivateMounts=
##         > PrivateTmp=
##         > UMask=
##         > RuntimeDirectoryMode=0750
##         > NoExecPaths=/tmp
##         > BindReadOnlyPaths=
##   [+] Systemd User + Group Hardening:
##         > PrivateUsers=
##         > User=$User
##         > DynamicUser=yes
##   [+] Systemd Network Hardening:
##         > PrivateNetwork=
##         > RestrictAddressFamilies=AF_UNIX AF_INET
##         > IPAddressDeny= 
##         > IPAddressAllow= 
##   [+] Systemd Special Hardening Parameters:
##         > AppArmorProfile=
##         > PAMName=
##         > SELinuxContext=
##         > RestrictNamespaces=uts ipc pid user cgroup net
##         > MemoryDenyWriteExecute=
##         > NoNewPrivileges=true
## ------------------------------------------------------ ##