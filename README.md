___
# ParrotSecWiki
___



[![pipeline status](https://gitlab.com/xe1phix/ParrotSecWiki/badges/InfoSecTalk/pipeline.svg)](https://gitlab.com/xe1phix/ParrotSecWiki/commits/InfoSecTalk)
[![coverage report](https://gitlab.com/xe1phix/ParrotSecWiki/badges/InfoSecTalk/coverage.svg)](https://gitlab.com/xe1phix/ParrotSecWiki/commits/InfoSecTalk)




<p align="center">
  <a href="https://archive.org/details/@xe1phix">
    <img src="https://img.shields.io/badge/Archive.org-%40Xe1phix-blue?style=plastic&logo=archiveofourown" alt="@Xe1phix">
  </a>
   <a href="https://gitlab.com/xe1phix/Gnupg/blob/master/Xe1phix.asc">
    <img src="https://img.shields.io/badge/Xe1phix's-GnuPG%20Key-red?style=flat&logo=gnu" alt="Xe1phix's GnuPG Key">
  </a>
  <a href="https://www.bitchute.com/channel/U0QCI90XuSH9/">
    <img src="https://img.shields.io/badge/Bitchute-%40Xe1phix-red?style=plastic&logo=youtube" alt="@Xe1phix Bitchute">
  </a>
  <a href="https://www.youtube.com/channel/UC4rzx4VToyHJDWbAEJ5cMxQ/videos">
    <img src="https://img.shields.io/badge/YouTube-%40Xe1phix-red?style=plastic&logo=youtube" alt="@Xe1phix YouTube">
  </a>
  <a href="https://peertube.video/accounts/xe1phix/videos">
    <img src="https://img.shields.io/badge/PeerTube.Video-%40Xe1phix-red?style=plastic&logo=youtube" alt="@Xe1phix PeerTube.Video">
  </a>
  <a href="https://peertube.live/accounts/xe1phix/videos">
    <img src="https://img.shields.io/badge/PeerTube.Live-%40Xe1phix-red?style=plastic&logo=youtube" alt="@Xe1phix PeerTube.Live">
  </a>
  <a href="https://open.tube/accounts/xe1phix/videos">
    <img src="https://img.shields.io/badge/Open.Tube-%40Xe1phix-red?style=plastic&logo=youtube" alt="@Xe1phix Open.Tube">
  </a>
  <a href="https://peertube.linuxrocks.online/accounts/xe1phix/videos">
    <img src="https://img.shields.io/badge/PeerTube.LinuxRocks-%40Xe1phix-red?style=plastic&logo=youtube" alt="@Xe1phix PeerTube.LinuxRocks">
  </a>
  <a href="https://dlive.tv/Xe1phix">
    <img src="https://img.shields.io/badge/DLive-%40Xe1phix-red?style=plastic&logo=youtube" alt="@Xe1phix DLive">
  </a>                          
</p>
___
<p align="center">
  <a href="https://secdsm.slack.com">
    <img src="https://img.shields.io/badge/Slack-%40Xe1phix-blueviolet?style=flat&logo=slack" alt="SecDSM Slack @Xe1phix">
  </a>
  <a href="https://gitlab.com/xe1phix/Gnupg/blob/master/Xe1phix-WireGnuPG.txt">
    <img src="https://img.shields.io/badge/Wire-%40Xe1phix-critical?style=flat&logo=tails" alt="Xe1phix's Wire Messenger GnuPG Key">
  </a>
  <a href="https://gitlab.com/xe1phix/Gnupg/blob/master/Xe1phix-InfoSecContact-v4.2.txt">
    <img src="https://img.shields.io/badge/%40Xe1phix-InfoSec_Contact-blue?style=plastic&logo=tor" alt="Xe1phix InfoSec-Contact">
  </a>
  <a href="mailto:xe1phix@protonmail.ch">
    <img src="https://img.shields.io/badge/Xe1phix-%40protonmail.ch-blue?style=plastic&logo=gnu" alt="ProtonMail - Xe1phix">
  </a>
  <a href="https://gitlab.com/xe1phix/Gnupg/blob/master/Xe1phix_protonmail.ch.asc">
    <img src="https://img.shields.io/badge/Xe1phix-%40protonmail.ch-blue?style=plastic&logo=gnu" alt="Xe1phix - ProtonMail Public Key">
  </a>
</p>